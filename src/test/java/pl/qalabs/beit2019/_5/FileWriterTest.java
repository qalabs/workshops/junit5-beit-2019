package pl.qalabs.beit2019._5;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FileWriterTest {

    FileWriter fileWriter = new FileWriter();

    @Test
    void writesContentToFile(@TempDir Path tempDir) throws IOException {
        // create new path where the contents will be stored
        Path output = null;

        // write to file
        fileWriter.writeTo(output.toString(), "lorem ipsum");

        // assert file contents contain what was written before
        assertAll(
                () -> assertTrue(Files.exists(output)),
                () -> assertLinesMatch(List.of("lorem ipsum"), Files.readAllLines(output))
        );
    }

    @Test
    void throwsErrorWhenTargetFileExists(@TempDir Path tempDir) throws IOException {
        // create new file
        Path output = Files.createFile(
                tempDir.resolve("output.txt")
        );

        // verifies exception was thrown when trying to write to an existing file
        IOException expectedException = assertThrows(IOException.class, () -> fileWriter.writeTo(output.toString(), "lorem "));
        assertEquals("file already exists", expectedException.getMessage());
    }

}