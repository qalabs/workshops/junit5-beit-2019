package pl.qalabs.beit2019._3;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Built-in parameterized tests
 */
class FizzBuzzParameterizedTest {

    FizzBuzz fizzBuzz = new FizzBuzz();

    @Test
    void fizzBuzzWithLoop() {

        Map<Integer, String> params = new HashMap<>();
        params.put(1, "1");
        params.put(2, "2");
        params.put(3, "Fizz");
        params.put(4, "4");
        params.put(6, "Fizz");
        params.put(5, "Buzz");
        params.put(10, "Buzz");
        params.put(15, "FizzBuzz");
        params.put(30, "FizzBuzz");

        params.forEach((number, result) -> Assertions.assertEquals(fizzBuzz.calculate(number), result));
    }

    // Value source

    @ParameterizedTest
    @ValueSource(ints = {3, 6, 9})
    void returnsFizzWhenNumberIsDivisibleBy3(int number) {
        Assertions.assertEquals(fizzBuzz.calculate(number), "Fizz");
    }

    @ParameterizedTest
    @ValueSource(ints = {5, 10, 20})
    void returnsBuzzWhenNumberIsDivisibleBy5(int number) {
        Assertions.assertEquals(fizzBuzz.calculate(number), "Buzz");
    }


    // Method source

    @ParameterizedTest(name = "returns {1} when given number is {0}")
    @MethodSource("fizzBuzzParams")
    void fizzBuzzWithMethodSource(int number, String result) {
        Assertions.assertEquals(fizzBuzz.calculate(number), result);
    }

    private static Stream<Arguments> fizzBuzzParams() {
        return Stream.of(
                Arguments.of(1, "1"),
                Arguments.of(2, "2"),
                Arguments.of(3, "Fizz"),
                Arguments.of(6, "Fizz"),
                Arguments.of(5, "Buzz"),
                Arguments.of(10, "Buzz"),
                Arguments.of(15, "FizzBuzz"),
                Arguments.of(30, "FizzBuzz")
        );
    }

    // CSV file source

    @ParameterizedTest(name = "returns {1} when given number is {0}")
    @CsvFileSource(resources = "/fizz-buzz.csv", numLinesToSkip = 1)
    void fizzBuzzWitCsvSource(int number, String result) {
        Assertions.assertEquals(fizzBuzz.calculate(number), result);
    }
}