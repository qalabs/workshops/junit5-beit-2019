package pl.qalabs.beit2019._2;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;

class _3_ExceptionsTest {

    class ExceptionThrower {
        String throwsException(int i) throws RuntimeException {
            if (i == 0) {
                return "0";
            }
            throw new RuntimeException("Unexpected error!");
        }
    }

    ExceptionThrower exceptionThrower = new ExceptionThrower();

    @Test
    void assertException() {
        // execute and get the exception
        RuntimeException thrown = assertThrows(
                RuntimeException.class, () -> exceptionThrower.throwsException(1)
        );
        // verify exception
        assertAll(
                () -> assertEquals("Unexpected error!", thrown.getMessage()),
                () -> assertNull(thrown.getCause())
        );
    }

    @Test
    void exceptionNotThrown() {
        // expected an exception, but nothing was thrown
        RuntimeException thrown = assertThrows(
                RuntimeException.class, () -> exceptionThrower.throwsException(0)
        );
    }

}
