package pl.qalabs.beit2019._2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class _1_BasicAssertionsTest {

    List<String> owners = List.of("Betty Davis", "Eduardo Rodriquez");

    @Test
    void builtInAssertions() {
        // assert not null
        assertNotNull(owners);

        // assert not null with message
        assertNotNull(owners, "Owners are expected to be not null");

        // assert equals
        assertEquals(2, owners.size(), "Found owner names size is incorrect");

        // assert line match
        assertLinesMatch(List.of("Betty Davis", "Eduardo Rodriquez"), owners);

        // assert array equals
        assertArrayEquals(
                new String[]{"Betty Davis", "Eduardo Rodriquez"},
                owners.toArray(new String[0])
        );
    }

}
