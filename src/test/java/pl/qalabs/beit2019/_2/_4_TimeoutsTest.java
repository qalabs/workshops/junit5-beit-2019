package pl.qalabs.beit2019._2;

import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

class _4_TimeoutsTest {

    class TaskExecutor {
        void execute(int seconds) {
            try {
                Thread.sleep(seconds * 1_000);
            } catch (InterruptedException ignored) {
            }
        }
    }

    TaskExecutor taskExecutor = new TaskExecutor();

    @Test
    void passesWhenTimoutNotExceeded() {
        assertTimeout(Duration.ofSeconds(2), () -> taskExecutor.execute(1));
    }

    @Test
    void failsWhenTimeoutExceededWaitingToFinishTask() {
        assertTimeout(Duration.ofSeconds(1), () -> taskExecutor.execute(5));
    }

    @Test
    void failsWhenTimeoutExceededForcingTaskToStop() {
        assertTimeoutPreemptively(Duration.ofSeconds(1), () -> taskExecutor.execute(5));
    }
}
