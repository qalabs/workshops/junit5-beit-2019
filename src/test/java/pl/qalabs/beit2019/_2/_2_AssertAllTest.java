package pl.qalabs.beit2019._2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class _2_AssertAllTest {

    List<String> owners = List.of("Betty Davis", "Eduardo Rodriquez");

    @Test
    void assertAll() {

        // group assertions with assert all
        Assertions.assertAll(
                () -> Assertions.assertTrue(owners.contains("Betty Doe"), "Contains Betty Doe"),
                () -> Assertions.assertTrue(owners.contains("Eduardo Rodriquez"), "Contains Eduardo Rodriquez"),
                () -> Assertions.assertTrue(owners.contains("John Doe"), "Contains John Doe")
        );
    }

}
