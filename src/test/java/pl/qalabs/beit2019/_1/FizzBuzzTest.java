package pl.qalabs.beit2019._1;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.DisabledOnOs;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;

/**
 * Basics
 */
@DisplayName("JUnit 5 at glance with FizzBuzz")
@TestMethodOrder(MethodOrderer.Alphanumeric.class)
class FizzBuzzTest {

    FizzBuzz fizzBuzz;

    @BeforeAll
    static void beforeAll() {

    }

    @AfterAll
    static void afterAll() {

    }

    @BeforeEach
    void beforeEach() {
        fizzBuzz = new FizzBuzz();
    }

    @AfterEach
    void afterEach() {
        fizzBuzz = null;
    }

    @Test
    void test() {
        Assertions.assertEquals(fizzBuzz.calculate(1), "1");
    }


    @Test
    @DisplayName("should return 'Fizz' when given number is divisible by '3'")
    void testWithDisplayNameAdjusted() {
        Assertions.assertEquals(fizzBuzz.calculate(3), "Fizz");
    }


    @Test
    @Disabled("Failing")
    void disabledFailingTestWithMessage() {
        Assertions.assertEquals(fizzBuzz.calculate(42), "FizzBuzz");
    }

    @RepeatedTest(value = 3)
    void repeatedTest() {
        Assertions.assertEquals(fizzBuzz.calculate(15), "FizzBuzz");
    }

    @RepeatedTest(value = 3)
    void repeatedTestWithParameterInjection(RepetitionInfo repetitionInfo) {
        System.out.println(repetitionInfo.getCurrentRepetition() + " of " + repetitionInfo.getTotalRepetitions());
        Assertions.assertEquals(fizzBuzz.calculate(30), "FizzBuzz");
    }

    @Test
    @DisabledOnOs(OS.MAC)
    void disableOnMac() {
        Assertions.assertEquals(fizzBuzz.calculate(45), "FizzBuzz");
    }

    @Test
    @EnabledOnOs(OS.WINDOWS)
    void enableOnWindows() {
        Assertions.assertEquals(fizzBuzz.calculate(45), "FizzBuzz");
    }

}