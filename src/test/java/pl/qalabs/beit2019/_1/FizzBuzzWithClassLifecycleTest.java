package pl.qalabs.beit2019._1;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.DisabledOnOs;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FizzBuzzWithClassLifecycleTest {

    FizzBuzz fizzBuzz;
    FizzBuzz anotherFizzBuzz;

    @BeforeAll
    void beforeAll() {
        fizzBuzz = new FizzBuzz();
        anotherFizzBuzz = fizzBuzz;
    }

    @BeforeEach
    void beforeEach() {
        Assertions.assertEquals(fizzBuzz, anotherFizzBuzz);
    }

    @Test
    void test1() {
        Assertions.assertEquals(fizzBuzz.calculate(1), "1");
    }

    @Test
    void test2() {
        Assertions.assertEquals(fizzBuzz.calculate(3), "Fizz");
    }

    @Test
    void test3() {
        Assertions.assertEquals(fizzBuzz.calculate(5), "Buzz");
    }

    @Test
    void test4() {
        Assertions.assertEquals(fizzBuzz.calculate(15), "FizzBuzz");
    }
}