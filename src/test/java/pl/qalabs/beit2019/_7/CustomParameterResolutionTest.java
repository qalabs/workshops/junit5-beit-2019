package pl.qalabs.beit2019._7;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(UserParameterResolver.class)
class CustomParameterResolutionTest {

    @Test
    void testWithUser(@UserCreator(name = "John Doe") User user) {
        Assertions.assertEquals(user.getName(), "John Doe");
        Assertions.assertEquals(user.getEmail(), "");
    }

    @Test
    void testWithAnotherUser(@UserCreator(name = "John Doe", email = "jd@example.com") User user) {
        Assertions.assertEquals(user.getName(), "John Doe");
        Assertions.assertEquals(user.getEmail(), "jd@example.com");
    }
}
