package pl.qalabs.beit2019._7;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface UserCreator {

    // required name attribute
    String name();

    // optional email attribute
    String email() default "";

}
