package pl.qalabs.beit2019._6;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;

import java.util.Optional;

/**
 * Basic test results watcher
 */
class PrintingTestWatcherExtension implements TestWatcher {

    @Override
    public void testDisabled(ExtensionContext context, Optional<String> reason) {
        System.out.println("Test aborted: " + context.getDisplayName() + " with reason: " + reason.orElse("No reason!"));
    }

    @Override
    public void testSuccessful(ExtensionContext context) {
        System.out.println("Success: " + context.getDisplayName());
    }

    @Override
    public void testAborted(ExtensionContext context, Throwable cause) {
        System.out.println("Aborted: " + cause.getMessage());
    }

    @Override
    public void testFailed(ExtensionContext context, Throwable cause) {
        System.out.println("Failed: " + cause.getMessage());
    }
}
