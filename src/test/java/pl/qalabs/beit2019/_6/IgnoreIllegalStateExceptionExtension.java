package pl.qalabs.beit2019._6;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestExecutionExceptionHandler;

/**
 * Extension that ignores {@link IllegalStateException} in tests
 */
class IgnoreIllegalStateExceptionExtension implements TestExecutionExceptionHandler {
    @Override
    public void handleTestExecutionException(ExtensionContext context, Throwable throwable) throws Throwable {
        if (throwable instanceof IllegalStateException) {
            return;
        }
        throw throwable;
    }
}
