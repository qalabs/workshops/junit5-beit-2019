package pl.qalabs.beit2019._6;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(PrintingTestWatcherExtension.class)
@ExtendWith(TestExecutionTimingExtension.class)
@ExtendWith(IgnoreIllegalStateExceptionExtension.class)
class TestWithMultipleHooksRegisteredTest {

    @Test
    @Disabled("No reason...")
    void disabledTest() {

    }

    @Test
    void testSuccessful() {

    }

    @Test
    void testAborted() {
        throw new RuntimeException("Aborted");
    }

    @Test
    void testFailed() {
        Assertions.assertTrue(Boolean.FALSE);
    }

    @Test
    void testWithIgnoredException() {
        // test passes with correctly registered extension
        throw new IllegalStateException("Should be ignored");
    }
}
