package pl.qalabs.beit2019._6;

import org.junit.jupiter.api.extension.*;

import java.lang.reflect.Method;

/**
 * Measures duration of each test method and prints it to console
 */
class TestExecutionTimingExtension implements
        BeforeTestExecutionCallback,
        AfterTestExecutionCallback {

    private static final String START_TIME_KEY = "start_time";

    @Override
    public void beforeTestExecution(ExtensionContext context) throws Exception {
        // store start time in a store
        getStore(context).put(START_TIME_KEY, System.currentTimeMillis());
    }

    @Override
    public void afterTestExecution(ExtensionContext context) throws Exception {
        // get start time from the store and remove the key
        long start = getStore(context).remove(START_TIME_KEY, long.class);
        // calculate duration
        long duration = System.currentTimeMillis() - start;
        // print result to console
        Method requiredTestMethod = context.getRequiredTestMethod();
        System.out.println("Method " + requiredTestMethod.getName() + " execution time: " + duration + " ms!");
    }

    private ExtensionContext.Store getStore(ExtensionContext context) {
        return context.getStore(ExtensionContext.Namespace.create(getClass(), context.getRequiredTestMethod()));
    }
}
