package pl.qalabs.beit2019._4;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class MockitoWithExtensionTest {

    @Mock
    Dependency dependency;

    @InjectMocks
    DummyService dummyService;

    @Test
    void verifiesCalculationServiceDelegatesToCalculator() {
        // when dependency called with "in" then return "in"
        Mockito.when(dependency.doSomethingWithString("in")).thenReturn("in");

        // assert dummy service surrounds output with "+"
        Assertions.assertEquals(dummyService.doSomething("in"), "+in+");

        // verify mock was executed
        Mockito.verify(dependency).doSomethingWithString("in");
    }
}
