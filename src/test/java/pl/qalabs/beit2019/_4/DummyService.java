package pl.qalabs.beit2019._4;

class DummyService {

    Dependency dependency;

    public DummyService(Dependency dependency) {
        this.dependency = dependency;
    }

    String doSomething(String in) {
        return "+" + dependency.doSomethingWithString(in) + "+";
    }
}
