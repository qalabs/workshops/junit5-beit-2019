package pl.qalabs.beit2019._4;

interface Dependency {
    String doSomethingWithString(String in);
}
